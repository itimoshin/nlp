import io
import os
import re
from string import punctuation

import pandas as pd
import nltk
nltk.download("stopwordsx")
from nltk.corpus import stopwords
from tqdm import tqdm



def clean_text(text, remove_stopwords=True):
    '''Clean the text, with the option to remove stopwords'''

    # Convert words to lower case and split them
    text = text.lower().split()

    # Optionally, remove stop words
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        text = [w for w in text if not w in stops]

    text = " ".join(text)

    # Clean the text
    text = re.sub(r"<br />", " ", text)
    text = re.sub(r"[^a-z]", " ", text)
    text = re.sub(r"   ", " ", text)  # Remove any extra spaces
    text = re.sub(r"  ", " ", text)

    # Remove punctuation from text
    text = ''.join([c for c in text if c not in punctuation])

    # Return a list of words
    return text


def logger_wrapper(func, index, arg, message, log_every: int):
    if index % log_every == 0:
        print("{}: {}".format(message, index))
    return func(arg)


def read_data():
    if not (os.path.isfile('./data/preparedLabeledTrainData.tsv') and os.path.isfile('./data/preparedTestData.tsv')):
        train = pd.read_csv("./data/labeledTrainData.tsv", delimiter="\t")
        test = pd.read_csv("./data/testData.tsv", delimiter="\t")
        clean_dataset(train)
        clean_dataset(test)
        train.to_csv("./data/preparedLabeledTrainData.tsv", sep="\t", index=False)
        test.to_csv("./data/preparedTestData.tsv", sep="\t", index=False)
    else:
        train = pd.read_csv("./data/preparedLabeledTrainData.tsv", delimiter="\t")
        test = pd.read_csv("./data/preparedTestData.tsv", delimiter="\t")
    return train, test


# def clean_datasets(train, test):
#     train['clean_review'] = train.apply(
#         lambda row: logger_wrapper(clean_text, row.name, row.get('review'), "clean-train", 500), axis=1)
#     test['clean_review'] = test.apply(
#         lambda row: logger_wrapper(clean_text, row.name, row.get('review'), "clean-test", 500), axis=1)


def clean_dataset(dataset):
    with tqdm(total=dataset.shape[0], desc='Cleaning texts for train set') as pbar:
        clean_data = []
        for review in dataset['review']:
            clean_data.append(clean_text(review, True))
            pbar.update(1)
        dataset['clean_review'] = clean_data
