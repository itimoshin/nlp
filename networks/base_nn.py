import os
import pickle
from abc import ABC
from collections import namedtuple

import tensorflow as tf
from keras_preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from tensorflow import Session
from tqdm import tqdm
import numpy as np

import common
import datasets
import embedded_tokenizer
from options import RnnOptions
from w2v.w2v_factory_new import get_w2v


def make_cell(lstm_size, keep_prob):
    lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(lstm_size)
    lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=keep_prob)
    return lstm_cell


class BaseNN(ABC):
    pred_model = None#: object
    train_model = None #: object
    options = None #: RnnOptions

    def __init__(self, graph, options: RnnOptions):
        self.options = options
        self.graph = graph

    def train(self, dataset, epochs):
        saver = tf.train.Saver()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        with tf.Session(graph=self.graph, config=config) as sess:
            sess.run(tf.global_variables_initializer())

            # Used to determine when to stop the training early
            valid_loss_history = []

            model_path = "./saved/{}".format(self.train_model.options.path())
            tokenizer_path = "{}/tokenizer".format(model_path)
            run_number = common.get_run_number(model_path)
            attempt_path = "{}/{}".format(model_path, run_number)
            checkpoint_path = "{}/{}/checkpoints/checkpoint.ckpt".format(model_path, run_number)
            prev_checkpoint_path = "{}/{}/checkpoints/checkpoint.ckpt" \
                .format(model_path, run_number - 1)

            if os.path.isfile(prev_checkpoint_path+'.index') and os.path.isfile(tokenizer_path):
                saver.restore(sess, prev_checkpoint_path)
                with open(tokenizer_path, 'rb') as tokenizer_file:
                    tokenizer = pickle.load(tokenizer_file)
            else:
                tokenizer, weights = get_w2v(self.options, dataset)
                # sess.run(tf.assign(self.train_model.embedding, weights, validate_shape=True))

            embedded_tokenizer.tokenize(tokenizer, dataset, self.options.max_len)

            # Keep track of which batch iteration is being trained
            iteration = 0

            print("Training Model: {}".format(self.train_model.options.path()))
            train_writer = tf.summary.FileWriter('{}/logs/train'.format(attempt_path), sess.graph)
            valid_writer = tf.summary.FileWriter('{}/logs/valid'.format(attempt_path), sess.graph)

            for e in range(epochs):
                state = sess.run(self.train_model.init_state)
                train_set, valid_set = train_test_split(dataset, test_size=0.15, random_state=2)

                # Record progress with each epoch
                train_loss = []
                train_acc = []
                val_acc = []
                val_loss = []

                with tqdm(total=len(train_set)) as pbar:
                    for batch_i, batch in enumerate(common.get_batches(train_set, self.train_model.options.batch_size), 1):
                        feed = {self.train_model.inputs: np.array([np.array(x) for x in batch['sequence_review']]),
                                self.train_model.labels: batch.get('sentiment')[:, None],
                                self.train_model.keep_prob: self.train_model.options.dropout,
                                self.train_model.init_state: state}
                        summary, loss, acc, state, _ = sess.run([self.train_model.merged,
                                                                 self.train_model.cost,
                                                                 self.train_model.accuracy,
                                                                 self.train_model.final_state,
                                                                 self.train_model.optimizer],
                                                                feed_dict=feed)

                        # Record the loss and accuracy of each training batch
                        train_loss.append(loss)
                        train_acc.append(acc)

                        # Record the progress of training
                        train_writer.add_summary(summary, iteration)

                        iteration += 1
                        pbar.update(self.train_model.options.batch_size)

                # Average the training loss and accuracy of each epoch
                avg_train_loss = np.mean(train_loss)
                avg_train_acc = np.mean(train_acc)

                val_state = sess.run(self.train_model.init_state)
                with tqdm(total=len(valid_set)) as pbar:
                    for batch in common.get_batches(valid_set, self.train_model.options.batch_size):
                        feed = {self.train_model.inputs: np.array([np.array(x) for x in batch['sequence_review']]),
                                self.train_model.labels: batch.get('sentiment')[:, None],
                                self.train_model.keep_prob: 1,
                                self.train_model.init_state: val_state}
                        summary, batch_loss, batch_acc, val_state = sess.run([self.train_model.merged,
                                                                              self.train_model.cost,
                                                                              self.train_model.accuracy,
                                                                              self.train_model.final_state],
                                                                             feed_dict=feed)

                        # Record the validation loss and accuracy of each epoch
                        val_loss.append(batch_loss)
                        val_acc.append(batch_acc)
                        pbar.update(self.train_model.options.batch_size)

                # Average the validation loss and accuracy of each epoch
                avg_valid_loss = np.mean(val_loss)
                avg_valid_acc = np.mean(val_acc)
                valid_loss_history.append(avg_valid_loss)
                # Record the validation data's progress
                valid_writer.add_summary(summary, iteration)
                # Print the progress of each epoch
                print("Epoch: {}/{}".format(e, epochs),
                      "Train Loss: {:.3f}".format(avg_train_loss),
                      "Train Acc: {:.3f}".format(avg_train_acc),
                      "Valid Loss: {:.3f}".format(avg_valid_loss),
                      "Valid Acc: {:.3f}".format(avg_valid_acc))

                # Stop training if the validation loss does not decrease after 3 epochs
                if avg_valid_loss > min(valid_loss_history):
                    print("No Improvement.")
                    stop_early += 1
                    if stop_early == 100:
                        break

                        # Reset stop_early if the validation loss finds a new low
                # Save a checkpoint of the model
                else:
                    print("New Record!")
                    stop_early = 0
                    saver.save(sess, checkpoint_path)

    def make_prediction(self, input_seq):
        model_path = self.options.path()
        self.options.batch_size = 1
        pred_model = self._build_pred_model(self.options, )

        last_attempt = common.get_run_number(model_path)
        checkpoint_path = "./saved/{}/{}/checkpoints/checkpoint.ckpt".format(model_path, last_attempt)
        with tf.Session(graph=self.graph) as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()  # Gets all variables in `graph`.
            saver.restore(sess, checkpoint_path)
            tf.train.latest_checkpoint('./')
            test_state = sess.run(self.pred_model.init_state)

            feed = {self.pred_model.inputs: input_seq,
                    self.pred_model.keep_prob: 1,
                    self.pred_model.init_state: test_state}
            prediction = sess.run(pred_model.predictions, feed_dict=feed)
            return prediction

    def _build_pred_model(self, options: RnnOptions) -> None:
        raise Exception("Not implemented")

    def _build_train_model(self):
        labels = tf.placeholder(name='labels', dtype=tf.int32, shape=[None, None])

        tf.summary.histogram('predictions', self.pred_model.predictions)

        cost = tf.losses.mean_squared_error(labels, self.pred_model.predictions)
        tf.summary.scalar('cost', cost)

        optimizer = tf.train.AdamOptimizer(self.options.learning_rate).minimize(cost)

        correct_pred = tf.equal(tf.cast(tf.round(self.pred_model.predictions), tf.int32), labels)
        accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
        tf.summary.scalar('accuracy', accuracy)

        # Merge all of the summaries
        merged = tf.summary.merge_all()

        export_nodes = ['labels', 'accuracy', 'cost', 'optimizer', 'merged']

        rnn_model_dict = self.pred_model._asdict()
        local_dict = locals()
        train_model_dict = dict([(i, local_dict[i]) for i in export_nodes])
        merged_model_dict = {**rnn_model_dict, **train_model_dict}
        return namedtuple("Graph", merged_model_dict.keys())(*merged_model_dict.values())



