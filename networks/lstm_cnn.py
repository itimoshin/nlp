from collections import namedtuple

import tensorflow as tf

from networks.base_nn import BaseNN
from options import RnnOptions


def make_cell(lstm_size, keep_prob):
    lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(lstm_size)
    lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=keep_prob)
    return lstm_cell


class LstmCNN(BaseNN):

    def __init__(self, options: RnnOptions):
        super().__init__(options)
        self.pred_model = self._build_pred_model(options)
        self.train_model = self._build_train_model()

    def _build_pred_model(self, options: RnnOptions) -> object:
        num_filters = 32
        sequence_length = 200
        cnn_emb_size = 256
        filter_sizes = [3, 4, 5]
        dropout_keep_prob = 0.5

        # one-hot vector per word in text -> matrix
        inputs = tf.placeholder(name='inputs', dtype=tf.int32, shape=[options.batch_size, sequence_length])
        embed_weights = embed_weights_arg
        embed = tf.nn.embedding_lookup(embed_weights, inputs)

        keep_prob = tf.placeholder(tf.float32, name='keep_prob')
        lstm_net = tf.nn.rnn_cell.MultiRNNCell(
            [make_cell(options.lstm_size, keep_prob) for _ in range(options.n_layers)])
        init_state = lstm_net.zero_state(options.batch_size, tf.float32)

        outputs, final_state = tf.nn.dynamic_rnn(lstm_net, embed,
                                                 initial_state=init_state)

        lstm_out_expanded = tf.expand_dims(outputs, -1)

        # 2. CONVOLUTION LAYER + MAXPOOLING LAYER (per filter) ###############################
        pooled_outputs = []
        for i, filter_size in enumerate(filter_sizes):
            with tf.name_scope("conv-maxpool-%s" % filter_size):
                # CONVOLUTION LAYER
                filter_shape = [filter_size, cnn_emb_size, 1, num_filters]
                W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
                b = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
                conv = tf.nn.conv2d(lstm_out_expanded, W, strides=[1, 1, 1, 1], padding="VALID", name="conv")
                # NON-LINEARITY
                h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                # MAXPOOLING
                pooled = tf.nn.max_pool(h, ksize=[1, sequence_length - filter_size + 1, 1, 1], strides=[1, 1, 1, 1],
                                        padding='VALID', name="pool")
                pooled_outputs.append(pooled)

        num_filters_total = num_filters * len(filter_sizes)
        h_pool = tf.concat(pooled_outputs, 3)
        h_pool_flat = tf.reshape(h_pool, [-1, num_filters_total])

        # #3. DROPOUT LAYER ###################################################################
        with tf.name_scope("dropout"):
            h_drop = tf.nn.dropout(h_pool_flat, dropout_keep_prob)

        # Initialize the weights and biases
        weights = tf.truncated_normal_initializer(stddev=0.1)
        biases = tf.zeros_initializer()

        dense = tf.contrib.layers.fully_connected(h_drop,
                                                  num_outputs=options.fc_units,
                                                  activation_fn=tf.sigmoid,
                                                  weights_initializer=weights,
                                                  biases_initializer=biases)

        predictions = tf.contrib.layers.fully_connected(dense,
                                                        num_outputs=1,
                                                        activation_fn=tf.sigmoid,
                                                        weights_initializer=weights,
                                                        biases_initializer=biases)

        # Export the nodes
        export_nodes = ['options', 'inputs', 'keep_prob', 'init_state', 'final_state', 'predictions']
        Graph = namedtuple('Graph', export_nodes)
        local_dict = locals()
        return Graph(*[local_dict[each] for each in export_nodes])
