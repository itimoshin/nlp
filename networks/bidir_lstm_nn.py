import os
import numpy as np
from collections import namedtuple

import tensorflow as tf
from sklearn.model_selection import train_test_split
from tqdm import tqdm

import common
from networks.base_nn import BaseNN
from options import RnnOptions


def make_cell(lstm_size, keep_prob):
    lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(lstm_size)
    lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=keep_prob)
    return lstm_cell


class BidirLstmNN(BaseNN):

    def __init__(self, options: RnnOptions, embed_weights_arg):
        super().__init__(options)
        self.pred_model = self._build_pred_model(options, embed_weights_arg)
        self.train_model = super()._build_train_model(embed_weights_arg)

    def _build_pred_model(self, options: RnnOptions, embed_weights_arg) -> object:
        # one-hot vector per word in text -> matrix
        inputs = tf.placeholder(name='inputs', dtype=tf.int32, shape=[None, None])
        embed_weights = embed_weights_arg
        embed = tf.nn.embedding_lookup(embed_weights, inputs)

        keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        lstm_net_fw = tf.nn.rnn_cell.MultiRNNCell(
            [make_cell(options.lstm_size, keep_prob) for _ in range(options.n_layers)])
        init_state_fw = lstm_net_fw.zero_state(options.batch_size, tf.float32)

        lstm_net_bw = tf.nn.rnn_cell.MultiRNNCell(
            [make_cell(options.lstm_size, keep_prob) for _ in range(options.n_layers)])
        init_state_bw = lstm_net_bw.zero_state(options.batch_size, tf.float32)

        outputs, final_state = tf.nn.bidirectional_dynamic_rnn(lstm_net_fw, lstm_net_bw, embed,
                                                               initial_state_fw=init_state_fw,
                                                               initial_state_bw=init_state_bw)

        # Initialize the weights and biases
        weights = tf.truncated_normal_initializer(stddev=0.1)
        biases = tf.zeros_initializer()

        dense = tf.contrib.layers.fully_connected(tf.concat(outputs, 2)[:, -1],
                                                  num_outputs=options.fc_units,
                                                  activation_fn=tf.sigmoid,
                                                  weights_initializer=weights,
                                                  biases_initializer=biases)

        predictions = tf.contrib.layers.fully_connected(dense,
                                                        num_outputs=1,
                                                        activation_fn=tf.sigmoid,
                                                        weights_initializer=weights,
                                                        biases_initializer=biases)

        # Export the nodes
        export_nodes = ['options', 'inputs', 'keep_prob', 'init_state_fw', 'init_state_bw', 'final_state',
                        'predictions']
        Graph = namedtuple('Graph', export_nodes)
        local_dict = locals()
        graph = Graph(*[local_dict[each] for each in export_nodes])

        return graph

    def train(self, dataset, epochs):
        saver = tf.train.Saver()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        with tf.Session(config=config) as sess:
            sess.run(tf.global_variables_initializer())

            # Used to determine when to stop the training early
            valid_loss_history = []

            model_path = "./saved/{}".format(self.train_model.options.path())
            run_number = common.get_run_number(model_path)
            attempt_path = "{}/{}".format(model_path, run_number)
            checkpoint_path = "{}/{}/checkpoints/checkpoint.ckpt".format(model_path, run_number)
            prev_checkpoint_path = "{}/{}/checkpoints/checkpoint.ckpt" \
                .format(model_path, run_number - 1)

            if os.path.isfile(prev_checkpoint_path+'.index'):
                saver.restore(sess, prev_checkpoint_path)

            # Keep track of which batch iteration is being trained
            iteration = 0

            print("Training Model: {}".format(self.train_model.options.path()))
            train_writer = tf.summary.FileWriter('{}/logs/train'.format(attempt_path), sess.graph)
            valid_writer = tf.summary.FileWriter('{}/logs/valid'.format(attempt_path), sess.graph)

            for e in range(epochs):
                state_fw = sess.run(self.train_model.init_state_fw)
                state_bw = sess.run(self.train_model.init_state_bw)
                train_set, valid_set = train_test_split(dataset, test_size=0.15, random_state=2)

                # Record progress with each epoch
                train_loss = []
                train_acc = []
                val_acc = []
                val_loss = []

                with tqdm(total=len(train_set)) as pbar:
                    for batch_i, batch in enumerate(common.get_batches(train_set, self.train_model.options.batch_size), 1):
                        feed = {self.train_model.inputs: np.array([np.array(x) for x in batch['sequence_review']]),
                                self.train_model.labels: batch.get('sentiment')[:, None],
                                self.train_model.keep_prob: self.train_model.options.dropout,
                                self.train_model.init_state_fw: state_fw,
                                self.train_model.init_state_bw: state_bw}
                        summary, loss, acc, state, _ = sess.run([self.train_model.merged,
                                                                 self.train_model.cost,
                                                                 self.train_model.accuracy,
                                                                 self.train_model.final_state,
                                                                 self.train_model.optimizer],
                                                                feed_dict=feed)

                        # Record the loss and accuracy of each training batch
                        train_loss.append(loss)
                        train_acc.append(acc)

                        # Record the progress of training
                        train_writer.add_summary(summary, iteration)

                        iteration += 1
                        pbar.update(self.train_model.options.batch_size)

                # Average the training loss and accuracy of each epoch
                avg_train_loss = np.mean(train_loss)
                avg_train_acc = np.mean(train_acc)

                val_state_fw = sess.run(self.train_model.init_state_fw)
                val_state_bw = sess.run(self.train_model.init_state_bw)
                with tqdm(total=len(valid_set)) as pbar:
                    for batch in common.get_batches(valid_set, self.train_model.options.batch_size):
                        feed = {self.train_model.inputs: np.array([np.array(x) for x in batch['sequence_review']]),
                                self.train_model.labels: batch.get('sentiment')[:, None],
                                self.train_model.keep_prob: 1,
                                self.train_model.init_state_fw: val_state_fw,
                                self.train_model.init_state_bw: val_state_bw}
                        summary, batch_loss, batch_acc, val_state = sess.run([self.train_model.merged,
                                                                              self.train_model.cost,
                                                                              self.train_model.accuracy,
                                                                              self.train_model.final_state],
                                                                             feed_dict=feed)

                        # Record the validation loss and accuracy of each epoch
                        val_loss.append(batch_loss)
                        val_acc.append(batch_acc)
                        pbar.update(self.train_model.options.batch_size)

                # Average the validation loss and accuracy of each epoch
                avg_valid_loss = np.mean(val_loss)
                avg_valid_acc = np.mean(val_acc)
                valid_loss_history.append(avg_valid_loss)
                # Record the validation data's progress
                valid_writer.add_summary(summary, iteration)
                # Print the progress of each epoch
                print("Epoch: {}/{}".format(e, epochs),
                      "Train Loss: {:.3f}".format(avg_train_loss),
                      "Train Acc: {:.3f}".format(avg_train_acc),
                      "Valid Loss: {:.3f}".format(avg_valid_loss),
                      "Valid Acc: {:.3f}".format(avg_valid_acc))

                # Stop training if the validation loss does not decrease after 3 epochs
                if avg_valid_loss > min(valid_loss_history):
                    print("No Improvement.")
                    stop_early += 1
                    if stop_early == 100:
                        break

                        # Reset stop_early if the validation loss finds a new low
                # Save a checkpoint of the model
                else:
                    print("New Record!")
                    stop_early = 0
                    saver.save(sess, checkpoint_path)
