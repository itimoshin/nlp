from collections import namedtuple

import tensorflow as tf


from networks.base_nn import BaseNN
from options import RnnOptions


def make_cell(lstm_size, keep_prob):
    lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(lstm_size)
    lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=keep_prob)
    return lstm_cell


class LstmNN(BaseNN):

    def __init__(self, graph, options: RnnOptions):
        super().__init__(graph, options)
        self.pred_model = self._build_pred_model(options)
        self.train_model = super()._build_train_model()

    def _build_pred_model(self, options: RnnOptions) -> object:
        with tf.Session(graph=self.graph) as sess:
            # one-hot vector per word in text -> matrix
                inputs = tf.placeholder(name='inputs', dtype=tf.int32, shape=[None, None])

                embedding = tf.Variable(dtype=tf.dtypes.float32,
                                        initial_value=tf.zeros(shape=[options.w2v.words_count, 300]))
                embed = tf.nn.embedding_lookup(embedding, inputs)

                keep_prob = tf.placeholder(tf.float32, name='keep_prob')
                lstm_net = tf.nn.rnn_cell.MultiRNNCell(
                    [make_cell(options.lstm_size, keep_prob) for _ in range(options.n_layers)])
                init_state = lstm_net.zero_state(options.batch_size, tf.float32)

                outputs, final_state = tf.nn.dynamic_rnn(lstm_net, embed,
                                                         initial_state=init_state)

                # Initialize the weights and biases
                weights = tf.truncated_normal_initializer(stddev=0.1)
                biases = tf.zeros_initializer()

                dense = tf.contrib.layers.fully_connected(outputs[:, -1],
                                                          num_outputs=options.fc_units,
                                                          activation_fn=tf.sigmoid,
                                                          weights_initializer=weights,
                                                          biases_initializer=biases)

                predictions = tf.contrib.layers.fully_connected(dense,
                                                                num_outputs=1,
                                                                activation_fn=tf.sigmoid,
                                                                weights_initializer=weights,
                                                                biases_initializer=biases)

                # Export the nodes
                export_nodes = ['options', 'embedding', 'inputs', 'keep_prob', 'init_state', 'final_state', 'predictions']
                Graph = namedtuple('Graph', export_nodes)
                local_dict = locals()
                graph = Graph(*[local_dict[each] for each in export_nodes])
                return graph
