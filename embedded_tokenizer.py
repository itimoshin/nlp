import os
import pickle

from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer
import pandas as pd


# def create_or_restore_tokenizer(dataset, path):
#     if os.path.isfile(path):
#         return restore_tokenizer(path)
#     else:
#         tokenizer = Tokenizer()
#         tokenizer.fit_on_texts(dataset.get('clean_review'))
#         os.makedirs(os.path.dirname(path), exist_ok=True)
#         with open(path, 'wb') as handle:
#             pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
#         return tokenizer, max(tokenizer.index_word)


def tokenize(tokenizer, dataset, max_text_len):
    train_seq = pad_sequences(tokenizer.texts_to_sequences(dataset.get('clean_review')), maxlen=max_text_len)
    dataset['sequence_review'] = [list(seq) for seq in train_seq]


# def restore_tokenizer(path):
#     with open(path, 'rb') as handle:
#         tokenizer = pickle.load(handle)
#         return tokenizer, max(tokenizer.index_word)
