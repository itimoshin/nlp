import os


def get_run_number(model_path):
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    prev_runs_numbers = list(map(lambda x: int(x),
                                 filter(lambda x: x.isdigit(), os.listdir(model_path))))
    if len(prev_runs_numbers) is 0:
        return 1
    else:
        return max(prev_runs_numbers) + 1


def get_batches(dataset, batch_size):
    n_batches = len(dataset) // batch_size
    batches = [dataset[i * batch_size:i * batch_size + batch_size] for i in range(0, n_batches)]
    # batches[-1] = pd.concat([batches[-1], dataset[n_batches * batch_size:]])
    return batches


