from abc import ABC


class CnnOptions:

    def __init__(self, dict: dict) -> None:
        self.type = dict.get('type')
        self.batch_size = int(dict.get('batch_size'))
        self.learning_rate = float(dict.get('learning_rate'))

        self.embedded_size = int(dict.get('embedded_size'))
        self.n_words = int(dict.get('n_words'))

        self.lstm_size = int(dict.get('lstm_size'))
        self.dropout = float(dict.get('dropout'))
        self.n_layers = int(dict.get('n_layers'))
        self.w2v_type = dict.get('w2v_type')

        self.fc_units = int(dict.get('fc_units'))

    filter_sizes = None #: list


    def path(self):
        return "/".join(["{}={}".format(k, v) for k, v in self.__dict__.items()])

    @staticmethod
    def from_string(string):
        return RnnOptions(dict([i.split('=') for i in string.split('/')]))


class RnnOptions:
    def __init__(self, dict: dict) -> None:
        self.type = dict.get('type')
        self.batch_size = int(dict.get('batch_size'))
        self.learning_rate = float(dict.get('learning_rate'))

        self.embedded_size = int(dict.get('embedded_size'))
        self.max_len = int(dict.get('max_len'))

        self.lstm_size = int(dict.get('lstm_size'))
        self.dropout = float(dict.get('dropout'))
        self.n_layers = int(dict.get('n_layers'))
        self.w2v = dict.get('w2v')
        self.fc_units = int(dict.get('fc_units'))

    type = None #: str
    batch_size = None #: int
    learning_rate = None #: float
    embedded_size = None #: int
    max_len = None
    lstm_size = None #: int
    dropout = None #: float
    n_layers = None #: int
    fc_units = None #: int
    w2v = None #: W2VOptions

    def path(self):
        descr = '__'.join(["{}".format(v) for k, v in self.__dict__.items()])
        hash = self.__hash__()
        return '{}__{}'.format(hash, descr)

    def __hash__(self) -> int:
        return hash((self.type, self.batch_size, self.learning_rate,
                     self.embedded_size, self.max_len, self.w2v, self.lstm_size,
                     self.dropout, self.fc_units))


class W2VOptions(ABC):
    trainable = True
    words_count = 0


class W2VRandomOptions(W2VOptions):

    def __str__(self) -> str:
        return "Rnd{}Tr={}".format(self.words_count, self.trainable)


class W2VGoogleOptions(W2VOptions):

    def __str__(self) -> str:
        return "GooTr={}".format(self.trainable)


class W2VWikiOptions(W2VOptions):
    pass

    def __str__(self) -> str:
        return "WikiTr={}".format(self.trainable)


