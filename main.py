import tensorflow as tf

import datasets
from embedded_tokenizer import *
from networks.bidir_lstm_nn import BidirLstmNN
from networks.lstm_cnn import LstmCNN
from networks.lstm_nn import LstmNN
from options import RnnOptions, W2VRandomOptions, W2VGoogleOptions
from w2v.w2v_factory_new import get_w2v

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

train_set, test_set = datasets.read_data()
#max_text_len = 200
#tokenizer_path = "./saved/tokenizer"
#tokenizer, n_words = create_or_restore_tokenizer(train_set, tokenizer_path)
#tokenize(tokenizer, train_set, max_text_len)

print("TensorFlow version ", tf.version.VERSION)

max_text_len_options = [200, 300]
lstm_size_options = [128, 256]


# w2v_train = W2VRandomOptions()
# w2v_train.trainable = False
# w2v_train.words_count = 73441

w2v_train = W2VRandomOptions()
w2v_train.trainable = False
w2v_train.words_count = 73270


train_options = RnnOptions(
    {'type': 'lstm',
     'embedded_size': 300,
     'batch_size': 512,
     'lstm_size': 256,
     'dropout': 0.5,
     'n_layers': 2,
     'max_len': 200,
     'w2v': w2v_train,
     'fc_units': 512,
     'learning_rate': 0.001})

with tf.Graph().as_default() as graph:
    network = None #: BaseNN

    if train_options.type == 'lstm-cnn':
        network = LstmCNN(train_options)
    elif train_options.type == 'lstm':
        network = LstmNN(graph, train_options)
    elif train_options.type == 'bidir-lstm':
        network = BidirLstmNN(train_options)
    else:
        raise Exception('Illegal model type {}'.format(train_options.type))

    network.train(train_set, 10)
    network.make_prediction(input_seq)
