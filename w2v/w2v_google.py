import pickle

import gensim
import numpy as np

from keras_preprocessing.text import Tokenizer
from tqdm import tqdm

from w2v.w2v_base import W2VBase


class W2VGoogle(W2VBase):
    __limit = 200000

    def _load_new(self, path, tokenizer: Tokenizer):
        num_d = 300
        tokenizer_n_words = max(tokenizer.index_word)
        w2v_model = gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin',
                                                                    limit=self.__limit, binary=True)
        embeded_matrix = np.empty(shape=[tokenizer_n_words, num_d])

        with tqdm(total=self.__limit, desc="Generating an embedded matrix from w2v") as pbar:
            for i, (word, vocab) in enumerate(w2v_model.vocab.items()):
                word_index = tokenizer.word_index.get(word)
                if word_index is not None:
                    word_vector = w2v_model.vectors[vocab.index]
                    embeded_matrix[word_index - 1] = word_vector

                pbar.update(1)

        with tqdm(total=self.__limit, desc="Filling empty row of the embedded matrix with random values") as pbar:
            for i, row in enumerate(embeded_matrix):
                if row.max() == row.min() == 0:
                    embeded_matrix[i] = np.random.uniform(-1, 1, size=(1, num_d))
                pbar.update(1)

        with open(path, 'wb') as handle:
            pickle.dump(embeded_matrix, handle, protocol=0)
        return embeded_matrix

    def name(self):
        return 'google_{}'.format(self.__limit)
