import errno
import os
import pickle
from abc import ABC

from keras_preprocessing.text import Tokenizer
from pandas import DataFrame


class W2VBaseNew(ABC):

    w2v_paths = None



    """
    1) trainset.words = random_weights()
    2) google.words + trainset.words
    2) tokenizer.fit_on_texts -> получаем токинайзер и сохраняем его
    3) по словарю tokenizer создаем матрицу весов (либо из гугла, либо генерируем рандомную)
    """

    def __init__(self, w2vs_path) -> None:
        full_path = '{}/{}'.format(w2vs_path, self.name())
        tokenizer_path = '{}/tokenizer'.format(full_path)
        weights_path = '{}/weights'.format(full_path)
        self.w2v_paths = W2VPaths(full_path, tokenizer_path, weights_path)

    def load(self, train_set: DataFrame):
        # if os.path.isfile(self.w2v_paths.tokenizer_path) and os.path.isfile(self.w2v_paths.weights_path):
        #     with open(self.w2v_paths.tokenizer_path, 'rb') as tokenizer_file:
        #         with open(self.w2v_paths.weights_path, 'rb') as weights_file:
        #             tokenizer = pickle.load(tokenizer_file)
        #             weights = pickle.load(weights_file)
        #             return tokenizer, weights
        # else:
        return self._load_new(train_set)

    # def _save_weights_and_tokenizer(self, weights, tokenizer):
    #     if not os.path.exists(os.path.dirname(self.w2v_paths.weights_path)):
    #         try:
    #             os.makedirs(os.path.dirname(self.w2v_paths.weights_path))
    #         except OSError as exc:  # Guard against race condition
    #             if exc.errno != errno.EEXIST:
    #                 raise
    #
    #     with open(self.w2v_paths.weights_path, 'wb') as handle:
    #         pickle.dump(weights, handle, protocol=0)
    #
    #     with open(self.w2v_paths.tokenizer_path, 'wb') as handle:
    #         pickle.dump(tokenizer, handle, protocol=0)

    def _load_new(self, train_set: DataFrame):
        raise Exception("Not implemented")

    def name(self):
        raise Exception("Not implemented")


class W2VPaths:
    base_path = None
    tokenizer_path = None
    weights_path = None

    def __init__(self, base_path, tokenizer_path, weights_path) -> None:
        self.base_path = base_path
        self.tokenizer_path = tokenizer_path
        self.weights_path = weights_path


