import numpy as np
import pandas as pd
from keras_preprocessing.text import Tokenizer

from w2v.w2v_base_new import W2VBaseNew


class W2VRandom(W2VBaseNew):
    __limit = 200000


    """
    1) trainset.words = random_weights()
    2) google.words + trainset.words
    2) tokenizer.fit_on_texts -> получаем токинайзер и сохраняем его
    3) по словарю tokenizer создаем матрицу весов (либо из гугла, либо генерируем рандомную)
    """

    def __init__(self, w2vs_path) -> None:
        super().__init__(w2vs_path)

    def _load_new(self, train_set: pd.DataFrame):
        num_d = 300

        train_set_words = train_set.get('clean_review').map(lambda x: x.split()).apply(pd.Series).stack().unique()

        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(train_set_words)

        tokenizer_n_words = len(train_set_words)
        embed_matrix = np.random.uniform(-1, 1, (tokenizer_n_words, num_d))

        #self._save_weights_and_tokenizer(embed_matrix, tokenizer)

        return tokenizer, embed_matrix

    def name(self):
        return 'random'
