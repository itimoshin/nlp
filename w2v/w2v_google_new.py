import pickle

import gensim
import numpy as np
import os


from keras_preprocessing.text import Tokenizer

import pandas as pd
from tqdm import tqdm

from w2v.w2v_base import W2VBase
from w2v.w2v_base_new import W2VBaseNew


class W2VGoogleNew(W2VBaseNew):
    __limit = 1000


    """
    1) trainset.words = random_weights()
    2) google.words + trainset.words
    2) tokenizer.fit_on_texts -> получаем токинайзер и сохраняем его
    3) по словарю tokenizer создаем матрицу весов (либо из гугла, либо генерируем рандомную)
    """

    def __init__(self, w2vs_path) -> None:
        super().__init__(w2vs_path)

    def _load_new(self, train_set: pd.DataFrame):
        num_d = 300

        train_set_words = train_set.get('clean_review').map(lambda x: x.split()).apply(pd.Series).stack().unique()
        w2v_model = gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin',
                                                                    limit=self.__limit, binary=True)
        train_set_words = list(set(train_set_words) - set(w2v_model.index2word))

        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(train_set_words + w2v_model.index2word)

        tokenizer_n_words = len(train_set_words) + len(w2v_model.index2word)
        embed_matrix = np.empty(shape=[tokenizer_n_words, num_d])

        with tqdm(total=self.__limit, desc="Generating an embedded matrix from w2v") as pbar:
            for i, (word, vocab) in enumerate(w2v_model.vocab.items()):
                word_index = tokenizer.word_index.get(word)
                if word_index is not None:
                    word_vector = w2v_model.vectors[vocab.index]
                    embed_matrix[word_index - 1] = word_vector
                pbar.update(1)

        with tqdm(total=self.__limit, desc="Filling empty row of the embedded matrix with random values") as pbar:
            for i, word in enumerate(train_set_words):
                word_index = tokenizer.word_index.get(word)
                embed_matrix[word_index] = np.random.uniform(-1, 1, size=(1, num_d))
                pbar.update(1)

        #self._save_weights_and_tokenizer(embed_matrix, tokenizer)

        return tokenizer, embed_matrix

    def name(self):
        return 'google_{}'.format(self.__limit)
