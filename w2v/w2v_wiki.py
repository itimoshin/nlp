import io
import pickle

import numpy as np
from keras_preprocessing.text import Tokenizer
from tqdm import tqdm

from w2v.w2v_base import W2VBase


class W2VWiki(W2VBase):

    def _load_new(self, path, tokenizer: Tokenizer):
        tokenizer_n_words = max(tokenizer.index_word)
        fin = io.open("./data/wiki-news-300d-1M.vec", 'r', encoding='utf-8', newline='\n', errors='ignore')
        n, d = map(int, fin.readline().split())
        embeded_matrix = np.empty(shape=[tokenizer_n_words, d])

        with tqdm(total=n, desc="Generating an embedded matrix from w2v") as pbar:
            for i, line in enumerate(fin):
                tokens = line.rstrip().split(' ')
                word = tokens[0]

                word_index = tokenizer.word_index.get(word)
                if word_index is not None:
                    weights = list(map(float, tokens[1:]))
                    embeded_matrix[word_index - 1] = weights

                pbar.update(1)

        with tqdm(total=n, desc="Filling empty row of the embedded matrix with random values") as pbar:
            for i, row in enumerate(embeded_matrix):
                if row.max() == row.min() == 0:
                    embeded_matrix[i] = np.random.uniform(-1, 1, size=(1, d))
                pbar.update(1)

        with open('{}/{}'.format(path, self.name()), 'wb') as handle:
            pickle.dump(embeded_matrix, handle, protocol=0)
        return embeded_matrix

    def name(self):
        return 'w2v_wiki_{}'
