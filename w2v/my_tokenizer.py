from tqdm import tqdm


class MyTokenizer:
    index2word: [str]
    word2index: dict

    def __init__(self, words: [str]) -> None:
        self.index2word = words
        with tqdm(total=len(words), desc="new tokenizer") as pbar:
            for i, word in enumerate(words):
                self.word2index.update(word=i)
                pbar.update(1)
