from keras_preprocessing.text import Tokenizer
import tensorflow as tf

from options import RnnOptions
from w2v.w2v_google import W2VGoogle
from w2v.w2v_google_new import W2VGoogleNew
from w2v.w2v_wiki import W2VWiki


def get_w2v(options: RnnOptions, train_set):
    if options.w2v_type is None:
        raise Exception('Illegal w2v_type')

    trainable = True if options.w2v_type.endswith('_tr') else False
    tokenizer: Tokenizer = None
    if options.w2v_type.startswith('random'):
        embed_weights = tf.random_uniform((options.n_words, options.embedded_size), -1, 1)
        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(train_set)
    else:
        path = './saved/w2v'.format(options.w2v_type)
        if options.w2v_type.startswith('google'):
            tokenizer, embed_weights = W2VGoogleNew().load(path, train_set)
        #elif options.w2v_type.startswith('wiki'):
            #embed_weights = W2VWiki().load(path, tokenizer)
        else:
            raise Exception('Illegal w2v_type: {}'.format(options.w2v_type))

    embed_weights_variable = tf.Variable(name='embed_weights', dtype=tf.float32, trainable=trainable,
                                         initial_value=embed_weights)
    return tokenizer, embed_weights_variable
