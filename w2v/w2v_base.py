import os
import pickle
from abc import ABC

from keras_preprocessing.text import Tokenizer


class W2VBase(ABC):
    def load(self, path, tokenizer: Tokenizer):
        if os.path.isfile('{}/{}'.format(path, self.name())):
            with open(path, 'rb') as handle:
                embeded_matrix = pickle.load(handle)
                return embeded_matrix
        else:
            return self._load_new(path, tokenizer)

    def _load_new(self, path, tokenizer):
        raise Exception("Not implemented")

    def name(self):
        raise Exception("Not implemented")

