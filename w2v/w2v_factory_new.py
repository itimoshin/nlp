import tensorflow as tf
from pandas import DataFrame

from options import RnnOptions, W2VRandomOptions, W2VGoogleOptions, W2VWikiOptions
from w2v.w2v_google import W2VGoogle
from w2v.w2v_google_new import W2VGoogleNew
from w2v.w2v_random import W2VRandom
from w2v.w2v_wiki import W2VWiki


def get_w2v(options: RnnOptions, train_set: DataFrame):
    tokenizer = None
    embed_weights = None
    path = './saved'
    if isinstance(options.w2v, W2VRandomOptions):
        tokenizer, embed_weights = W2VRandom(path).load(train_set)
    elif isinstance(options.w2v, W2VGoogleOptions):
        tokenizer, embed_weights = W2VGoogleNew(path).load(train_set)
    elif isinstance(options.w2v, W2VWikiOptions):
            pass#passembed_weights = W2VWiki().load(path, tokenizer)

    assert options.w2v.words_count == embed_weights.shape[0]

    # embed_weights_variable = tf.Variable(name='embed_weights', dtype=tf.float32, trainable=options.w2v.trainable,
    #                                      initial_value=embed_weights)
    return tokenizer, embed_weights
